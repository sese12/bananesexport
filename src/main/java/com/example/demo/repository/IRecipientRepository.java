package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Recipient;

public interface IRecipientRepository extends JpaRepository<Recipient, Long> {


}
