package com.example.demo.controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Purchase;
import com.example.demo.model.Recipient;
import com.example.demo.repository.IPurchaseRepository;
import com.example.demo.repository.IRecipientRepository;
import com.example.demo.service.PurchaseService;
import com.example.demo.service.RecipientService;


@RestController
@RequestMapping("/api")
public class RecipientController {
	
	@Autowired
	private IRecipientRepository recipientRepository;
	
	@Autowired
	private RecipientService recipientService;

	@Autowired
	private IPurchaseRepository purchaseRepository;
	
	@Autowired
	private PurchaseService purchaseService;

	
    @GetMapping("/recipients")
	public List<Recipient> getRecipients() {
    	List<Recipient> recipient = recipientRepository.findAll();
		return recipient;
	}
    
    @GetMapping("/recipients/{id}")
	public Recipient getRecipientById(@PathVariable long id) {
		return recipientService.getById(id).orElse(null);
	}
    
    @GetMapping("/recipients/{id}/purchases")
	public Set<Purchase> getRecipientPurchases(@PathVariable long id){
   	  Optional<Recipient> recipient = recipientService.getById(id);
   	  return recipient.get().getPurchases();
	}
     
    @PostMapping("/recipients/create") 
	public Recipient createRecipient(@RequestBody Recipient recipient) {
		return  recipientService.save(recipient);
	}
	
    @PutMapping("/recipients/{id}")
    public Recipient updateRecipient(@RequestBody Recipient recipient) {
    	return recipientService.save(recipient);
    }
    
	@DeleteMapping("/recipients/{id}") 
	public void deleteRecipient(@PathVariable long id) {
		recipientService.deleteRecipient(id);
	}
}

