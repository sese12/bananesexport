package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Purchase;
import com.example.demo.repository.IPurchaseRepository;
import com.example.demo.service.PurchaseService;

@RestController
@RequestMapping("/api")
public class PurchaseController {
	
	@Autowired
	private IPurchaseRepository purchaseRepository;
	
	@Autowired
	private PurchaseService purchaseService;

    @GetMapping("/purchases")
	public List<Purchase> getPurchases() {
    	List<Purchase> purchase = purchaseRepository.findAll();
		return purchase;
	}
    
    @GetMapping("/purchases/{id}")
	public Purchase getPurchaseById(@PathVariable long id) {
		return purchaseService.getById(id).orElse(null);
	}
     
    @PostMapping("/purchases/create") 
	public Purchase createPurchase(@RequestBody Purchase purchase) {
		return  purchaseService.save(purchase);
	}
    
    @PutMapping("/purchases/{id}")
    public Purchase updatePurchase(@RequestBody Purchase purchase) {
    	return purchaseService.save(purchase);
    }
	
	@DeleteMapping("/purchases/{id}") 
	public void deletePurchase(@PathVariable long id) {
		purchaseService.deletePurchase(id);
	}
}
