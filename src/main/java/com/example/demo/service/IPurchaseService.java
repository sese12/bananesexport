package com.example.demo.service;

import java.util.Optional;

import com.example.demo.model.Purchase;

public interface IPurchaseService {
	
	Optional<Purchase> getById(long id);

	Purchase save(Purchase purchase);

	void deletePurchase(long id);
}
