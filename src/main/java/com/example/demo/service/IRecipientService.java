package com.example.demo.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.demo.model.Recipient;

@Service
public interface IRecipientService {
	
	Optional<Recipient> getById(long id);

	Recipient save(Recipient recipient);

	void deleteRecipient(long id);

}
