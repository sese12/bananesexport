package com.example.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Purchase;
import com.example.demo.repository.IPurchaseRepository;

@Service
public class PurchaseService implements IPurchaseService {
	
	@Autowired
	private IPurchaseRepository purchaseRepository;
	
	@Override
	public Optional<Purchase> getById(long id) {
		return purchaseRepository.findById(id);
	}

	@Override
	public Purchase save(Purchase purchase) {
		return purchaseRepository.save(purchase);
	}

	@Override
    public void deletePurchase(long id) {
		Optional<Purchase> purchase = purchaseRepository.findById(id);
        if (purchase.isPresent()) {
            purchaseRepository.delete(purchase.get());
        }
    }
}

