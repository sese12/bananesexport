package com.example.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Recipient;
import com.example.demo.repository.IRecipientRepository;

@Service("RecipientService")
public class RecipientService implements IRecipientService {
	
	@Autowired
	private IRecipientRepository recipientRepository;
	
	@Override
	public Optional<Recipient> getById(long id) {
		return recipientRepository.findById(id);
	}

	@Override
	public Recipient save(Recipient recipient) {
		return recipientRepository.save(recipient);
	}

	@Override
    public void deleteRecipient(long id) {
		Optional<Recipient> recipient = recipientRepository.findById(id);
        if (recipient.isPresent()) {
            recipientRepository.delete(recipient.get());
        }
    }

}
