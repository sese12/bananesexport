package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "purchase")
public class Purchase {
	
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	private long id;
	@NotNull
	@Column(nullable = false, name ="deliveryDate")
	private Date deliveryDate;
	@NotNull
	@Column(nullable = false, name ="quantity")
	private double quantity;
	@NotNull
	@Column(nullable = false, name ="price")
	private double price;
	@ManyToOne
	@JoinColumn(name = "recipient_id", nullable = false)
	private Recipient recipient;

	public Purchase() {
		
	}
	
	public Purchase(long id, @NotNull Date deliveryDate, @NotNull double quantity, @NotNull double price,
			Recipient recipient) {
		super();
		this.id = id;
		this.deliveryDate = deliveryDate;
		this.quantity = quantity;
		this.price = price;
		this.recipient = recipient;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

	public Recipient getRecipient() {
		return recipient;
	}

	public void setRecipient(Recipient recipient) {
		this.recipient = recipient;
	}

	@Override
	public String toString() {
		return "Purchase [id=" + id + ", deliveryDate=" + deliveryDate + ", quantity=" + quantity + ", price=" + price
				+ ", recipient=" + recipient + "]";
	}
	
}
