package com.example.demo.model;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "recipient")
public class Recipient {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id; 
	@NotNull
	@Size(min = 1, max = 30)
	@Column(nullable = false, name ="name")
	private String name; 
	@NotNull
	@Column(nullable = false, name ="address")
	private String adress;
	@NotNull
	@Column(nullable = false, name ="zipCode")
	private Integer zipCode; 
	@NotNull
	@Column(nullable = false, name ="city")
	private String city;
	@NotNull
	@Column(nullable = false, name ="country")
	private String country;
	@OneToMany(mappedBy = "recipient")
	private Set<Purchase> purchases = new HashSet<>();
		
	public Recipient(long id, @NotNull @Size(min = 1, max = 30) String name, @NotNull String adress,
			@NotNull int zipCode, @NotNull String city, @NotNull String country, HashSet<Purchase> purchases) {
		super();
		this.id = id;
		this.name = name;
		this.adress = adress;
		this.zipCode = zipCode;
		this.city = city;
		this.country = country;
		this.purchases = purchases;
	}
	
	public Recipient() {
		
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public int getZipCode() {
		return zipCode;
	}
	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public Set<Purchase> getPurchases() {
		return purchases;
	}

	public void setPurchases(Set<Purchase> purchases) {
		this.purchases = purchases;
	}

	@Override
	public String toString() {
		return "Recipient [id=" + id + ", name=" + name + ", adress=" + adress + ", zipCode=" + zipCode + ", city="
				+ city + ", country=" + country + ", purchases=" + purchases + "]";
	}

}
